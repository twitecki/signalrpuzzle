import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import * as signalR from '@aspnet/signalr-client';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'Signal R Demo Application';
  message = '';
  messages : Array<string>;
  chatConnection : signalR.HubConnection;

  constructor() {
    this.messages = ['test'];
  }

  sendMessage() {
    this.send(this.message);
  }

  ngOnInit() {
    console.log('inside the app component');

    var transportType = signalR.TransportType.WebSockets;
    var logger = new signalR.ConsoleLogger(signalR.LogLevel.Information);
    var chatHub = new signalR.HttpConnection(`http://${document.location.host}/chat`, { transport: transportType});
    this.chatConnection = new signalR.HubConnection(chatHub);
  
    this.chatConnection.onclose = e => {
        console.log('connection closed');
    };

    this.chatConnection.on('Send', (message) => {
      this.messages.push(message);
    });

    this.chatConnection.start().catch(err => {
        console.log('connection error');
    });
  }

  send(message) {
      this.chatConnection.invoke('Send', message);
  }
}
